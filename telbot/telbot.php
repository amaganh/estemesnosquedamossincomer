<?php 

$TOKEN = '1900292773:AAE4JkfasuFlMqxtchCUl6xAUfUZrVBcq9s';
$URL = "https://api.telegram.org/bot".$TOKEN;
$CHAT_IDS = __DIR__.'/gossips.php';

function add_chatid($id,$message = false){
    global $CHAT_IDS;
    if(file_exists($CHAT_IDS)){
        include_once($CHAT_IDS);

        if(in_array($id,$chat_ids))
            return;

        $chat_ids[] = $id;
        $cad = '';
        foreach ($chat_ids as $cid) {
            $cad .= ($cad == '') ? '' : ',';
            $cad .= $cid;
        }
        $cad = '<?php $chat_ids=['.$cad.']; ?>';
        unlink($CHAT_IDS);
    }
    else
        $cad = '<?php $chat_ids=['.$id.']; ?>';
    
    file_put_contents($CHAT_IDS,$cad);
    if($message)
        sendMessage($message,$id);
}

function get_key($id,$message){
    include_once(__DIR__.'/../utils/steps.php');
    $message = explode('/clave ',$message);
    if (count($message) == 2){
        $key = $message[1];
        if(isset($STEPS[$key])){
            if(isset($STEPS[$key]['key'])){
                if ($STEPS[$key]['key'] !== false){
                    $keys = implode(', ',$STEPS[$key]['key']);
                    sendMessage('La/s opcione/s son: '. $keys,$id);
                }
                else 
                    sendMessage('Ese paso no tiene clave, es troll',$id);
            }
        }
    }
}

function remove_chatid($id,$message = false){
    global $CHAT_IDS;
    if(file_exists($CHAT_IDS)){
        include_once($CHAT_IDS);
        $cad = '';
        foreach ($chat_ids as $cid) {
            
            if($cid == $id) continue;
            
            $cad .= ($cad == '') ? '' : ',';
            $cad .= $cid;
        }
        $cad = '<?php $chat_ids=['.$cad.']; ?>';
        unlink($CHAT_IDS);
        file_put_contents($CHAT_IDS,$cad);
    }
    if($message)
        sendMessage($message,$id);

}

function sendMessageToMultipleChats($text){
    global $CHAT_IDS;
    if(file_exists($CHAT_IDS)){
        include_once($CHAT_IDS);
        foreach ($chat_ids as $cid) {
            sendMessage($text,$cid);
        }
    }
}

function sendMessage($text,$id){
    global $URL;
    $json = ['chat_id'       => $id,
                'text'          => $text,
                'parse_mode'    => 'HTML'];

    $ch = curl_init($URL . '/sendMessage');
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, ($json));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);
}

?>