<?php
session_start();

include_once(__DIR__.'/utils/library.php');
include_once(__DIR__.'/utils/steps.php');
include_once(__DIR__.'/telbot/telbot.php');
include_once(__DIR__.'/telbot/resend.php');

$message = '';
if(isset($_GET['key']) && clean_keys($_GET['key']) == 'macarrones'){
  $_SESSION['login'] = true;
}

$login = false;
if(isset($_SESSION['login']) && $_SESSION['login']==true)
  $login = true;

$SEND = false;
if(isset($_GET['send']))
  $SEND = true;

if(isset($_GET['text']) && $_GET['text'] != '' && $login){
  $url = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
  header("Location: ".$url.'?send=true');
  sendMessageToMultipleChats($_GET['text']);
  die();
}

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>S & R</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
  </head>
  
  <body>
    <div class="wrapper">
      <div class="inner">
        <form action="" style="width:100%; padding-bottom:25px; padding-right:15px">
          <h3>S & R</h3>
          <div class="form-holder active">
            <?php if(!$login){ ?>
              <div class="form-text">
                Haz login con la palabra acordada
              </div>
              <input type="text" name="key" class="form-control">
            <?php } else{ 
              if($SEND) { ?>
                <h2> MESAJE ENVIADO</h2>
                <button>Escribir otro</button>
              <?php } else { ?>
                <p>Ya puedes enviar mensajes!</p>
                <textarea rows="10" name="text" class="form-control" style="height:auto"></textarea>
                <div class="form-login">
                    <button>Enviar</button>
                </div>
              <?php } } ?>
        </form>
      </div>
    </div>
  </body>
</html>
