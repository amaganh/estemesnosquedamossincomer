<?php

$STEPS = [
    [
        'text' => '<p> Inserta el codigo para avanzar</p>',
        'key' => 'null',
        'placeholder' =>'',
        'step' => 0,
    ],    
    [
        'text' => '<p>STEP Bienvenidos al juego!</p>
                    <p>  Si habéis llegado hasta aquí supongo que ya debéis saberos el nombre de nuestros familiares de memoria.</p> 
				    <p> Lamentablemente esto no nos asusta demasiado y el juego está empezando.... </p>
					<p>	Dentro de la caja de pasta debéis encontrar un macarrón con <span class="important-text">clave 1</span>, escribidla aquí </p>',
        'key' => ['25092021'],
        'placeholder' =>'',
        'image' => 'mano.png',
        'steps' => true
    ],
    [
        'text' => '<p>STEP Bien! Parece que ya sabéis como funciona esto!<br> Vamos a por la siguiente... <span class="important-text">clave 2</span></p>',
        'key' => ['dr mortis'],
        'placeholder' =>'',
        'shortcut' => 'boda',
        'image' => 'sergio.jpg',
        'steps' => true
    ],
    [
        'text' => '<p>STEP WOW! Sois unos expertos... Ya queda menos! <span class="important-text">clave 3</span> </p>',
        'key' => ['chupapijas','chupapija'],
        'placeholder' =>'',
        'shortcut' => 'tatuador',
        'image' => 'bata.png',
        'steps' => true
    ],
    [
        'text' => '<p>STEP Bien bien.... Pero.... ¿Cuántos macarrones había? </p>',
        'key' => false,
        'placeholder' =>'',
        'shortcut' => 'juan carlos',
        'image' => 'macarrones.jpeg',
        'steps' => true
    ],
    [
        'text' => '<p>STEP JAJAJA! nosotros tampoco tenemos ni idea...pero hablando en serio, imaginaos que sois Miguel... si sumamos las matrículas de los coches de Bruno, Faustino, Ángel, Rivas, Camilo, Díaz y Miguel, y restamos las de Estefi, Lucia, Carmen, Tiffany y el triciclo de Hugo... ¿Cual es el resultado?  </p>',
        'key' => ['20864'],
        'placeholder' =>'',
        'shortcut' => 'troll',
        'image' => 'matriculas.jpeg',
        'steps' => true
    ],
    [
        'text' => '<p>STEP Bien! parece que aprobáis mates! Espero que lo hayáis pasado muy bien por México y por la bonita pirámide Chichen Itza, que, por cierto, ¿Cuántos escalones tiene?</p>',
        'key' => ['365'],
        'placeholder' =>'',
        'shortcut' => 'matriculas',
        'image' => 'piramide.png',
        'steps' => 19
    ],
    [
        'text' => '<p>STEP ¿Cuántas veces llamaste gilipollas a Sergio subiendo? 
                <br>Suponemos que infinitas! Por cierto, que le pasa a la barra de progreso? va borracha?? Bueno, ya lo resolveré..., hablando de borrachos... ¿Como iba Faustino después de tomarse 372.423 chupitos?</p>',
        'key' => ['fatalix','fatalis','fatalis de la sierra','fatalix de la sierra'],
        'placeholder' =>'',
        'shortcut' => 'pasito a pasito',
        'image' => 'tete.png',
        'steps' => 19
    ],    
    [
        'text' => '<p>STEP [VOZ DE BORRACHO] [GARRASPEO DE GARGANTA] Y... Buenas, MAHOU hecha en Madrid, nacida para satisfacer a los paladares más exigentes, Mahou Cinco Estrellas está elaborada con las mejores variedades de lúpulo y levadura, que le imprimen sus característicos cuerpo y sabor. Mahou Cinco Estrellas destaca por su color dorado y característico amargor, moderado y fino, que da un buen balance gustativo que persiste en el paladar.</p>
        <p>¿En que año se lanza Mahou 5 estrellas?</p>',
        'key' => ['1969'],
        'placeholder' =>'',
        'shortcut' => 'melocoton',
        'image' => 'cerveza.jpg',
        'steps' => 19
    ],
    [
    'text' => '<p>STEP Y después de este gran discurso vamos con algunas preguntas,  Raquel, ¿Qué dirías para evitar un robo en Inglaterra?</p>',
        'key' => ['excuse me'],
        'placeholder' =>'',
        'shortcut' => 'estrella',
        'image' => 'robo.jpg',
        'steps' => 19
    ],
    [
    'text' => '<p>STEP Sergio,¿Qué le dirías a una pareja que se enrolla en una discoteca?</p>',
        'key' => ['follatela','follatela!','¡follatela!'],
        'placeholder' =>'',
        'shortcut' => 'london eye',
        'image' => 'follatela.jpg',
        'steps' => 19
    ],    
    [
        'text' => '<p>STEP ¿Cuántas empresas ha cerrado Estefi?</p>',
        'key' => false,
        'placeholder' =>'',
        'shortcut' => 'ultimo deseo',
        'image' => 'estefi.png',
        'steps' => 19
    ],
    [
        'text' => '<p>STEP Lo estáis haciendo bastante bien.... Vamos a daros una pista, si el dinero fuese de Rober Patata ¿Dónde lo guardaría?</p>',
        'key' => ['debajo de la cama', 'en la cama','cama','debajo del colchon','en el colchon','colchon','debajo del colchón','en el colchón','colchón'],
        'placeholder' =>'',
        'shortcut' => 'ultimo deseo',
        'image' => 'rober.jpg',
        'steps' => 19
    ],
    [
        'final' => true,
        'image' => 'final_2.jpg',
    ],
];

?>
