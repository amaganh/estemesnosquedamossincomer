<div class="wrapper">
    <div class="inner">
        <div class="image-holder">
            <img src="images/<? print $data['image']; ?>" alt="">
        </div>
        <form action="">
            <h3>S & R</h3>
            <? print_steps($data, $step); ?>
            <? print $message; ?>
            <div class="form-text">
                <? print str_replace('STEP','<span class="important-text">[ '.$step.' ]</span>',$data['text']); ?>
            </div>
            <div class="form-holder active">
                <?php if ($step == 0 ) : ?>
                    <input type="hidden" name="shortcut" value="true" class="form-control">
                <?php endif; ?>
                <input type="hidden" name="step" value="<? print $step; ?>" class="form-control">
                <input type="text" name="key" placeholder="<? print $data['placeholder']; ?>" class="form-control">
            </div>
            <div class="form-login">
                <button>Probar</button>
            </div>

            <?php if ($step == 0 ) : ?>
                <div class="form-text">
                    <p>
                        <a href="?">Volver a la página incial</a> 
                    </p>
                </div>
            <?php endif; ?>
            <?php if ($step !== 0 ) : ?>
                <div class="form-text">
                    <p>
                        Es posible que queráis continuar en otro momento, para ello, hemos habilitado atajos, puedes usarlos <a href="?shortcut=true">aquí</a>
                    </p>
                    <?php if (isset($data['shortcut'])) : ?>
                        <p>
                            El atajo a este nivel es: <span class="important-text"><?php print $data['shortcut']; ?></span>
                        </p>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <div class="form-text">
                <p>
                    En caso de bloqueo extremo, consultar con el eje del mal en: <a href="https://t.me/joinchat/vAVi1J5hLbk2ZDRk">@estemesnosquedamossincomer</a>
                </p>
            </div>
        </form>
    </div>
</div>