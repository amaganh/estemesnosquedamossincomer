<?php

function print_steps($data,$active){
	if(!isset($data['steps'])) return ;

	if(is_array($data['steps'])){
		$steps = $data['steps'][0];
		$active = $data['steps'][1];
	}
	elseif($data['steps'] != false){
		$steps = $data['steps'];
	}
	else {
		global $STEPS;
		$steps = count($STEPS)-1;
	}
    $cad = '';
    for ( $x = 1; $x < $steps; $x++ ) 
        $cad .= '<li class="'.(($active >= $x) ? 'active' : '').'"></li>';

    print '<ul class="steps">'.$cad.'</ul>';
}

function clean_keys($key){
	return strtolower( trim( $key ) );
}



?>