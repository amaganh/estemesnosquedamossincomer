<?php 
$dir = __DIR__.'/../images/final';
$images = '';
if ($gestor = opendir(__DIR__.'/../images/final')) {
    while (false !== ($entrada = readdir($gestor))) {
        if ($entrada != "." && $entrada != "..") {
            if(substr($entrada, -3) == 'mp4'){
                $img = "images/th/".substr($entrada,0, -3).'png';
                $images .= '<a data-video='."'".'{"source": [{"src":"images/final/'.$entrada.'", "type":"video/mp4"}]}'."'".'>
                <img class="img-responsive" src="'.$img.'" />
                </a>';
            }
            else
                $images .= '<a href="images/final/'.$entrada.'"><img src="images/final/'.$entrada.'" /></a>';
                //$images .= '<a href="images/final/'.$entrada.'"><video><source src="'.$entrada.'" type="video/mp4; codecs="avc1.42E01E, mp4a.40.2" /></video></a>';
        }
    }
    closedir($gestor);
}
?>

<div class="wrapper">
    <div class="inner">
        <div>
            <img src="images/<? print $data['image'];?>" style="max-width:100%" alt="">
            <h3>S & R</h3>
            <div class="form-text">
               <h5>¡Reto conseguido!</h5>
                <p>Esperamos que hayáis disfrutado resolviendolo tanto como nosotros lo hemos hecho preparándolo.</p>
                <p>Ya nos queda poco más que añadir... </p>
                <p>Simplemente deciros que ha sido un placer que nos hayáis dejado ser participes de un momento tan especial para vosotros.</p>
                <h6>¡Enhorabuena de nuevo!</h6>
                <p>Os dímos una pista con la pregunta de Rober, ¿mirasteis?</p>
                <p>Podéis encontrar la otra parte de nuestros sobres en el cabecero de la cama, ¡Espero que tengáis alicates!</p>
            </div>
            <ul id="animated-thumbnails-gallery">
                <?php echo $images; ?>
            </ul>

        </div>
    </div>
</div>