<?php
session_start();

include_once(__DIR__.'/utils/library.php');
include_once(__DIR__.'/utils/steps.php');
include_once(__DIR__.'/telbot/telbot.php');

$message = '';
if(isset($_GET['shortcut']) && $_GET['shortcut'] == true && !isset($_GET['key'])){
  $step = 0;
}
elseif(isset($_GET['shortcut']) && isset($_GET['key'])) {
  $shortcut = getShortcut($STEPS,$_GET['key']);

  if(!$shortcut){
    sendMessageToMultipleChats('[Error en Atajo] '.$_GET['key']);
    $message = '<div class="form-error"> Ops! Ese atajo no existe </div>';
  }else {
    $step = $shortcut;
    sendMessageToMultipleChats('[Atajo para clave ['.$step.'] '.$_GET['key']);
  }
} 
elseif(isset($_GET['key'],$_GET['step']) && is_numeric($_GET['step']) && isset($STEPS[$_GET['step']]) ) {
  
  sendMessageToMultipleChats('['.$_GET['step'].'] '.$_GET['key']);

  if( $STEPS[$_GET['step']]['key'] == false || in_array(clean_keys($_GET['key']),$STEPS[$_GET['step']]['key']))
    $step=$_GET['step']+1; 
  else
    $message = '<div class="form-error"> Ops!! No es lo que buscabamos </div>';
}
elseif (isset($_GET['step'],$_SESSION["step"]) && is_numeric($_GET['step']) && isset($STEPS[$_GET['step']]) && $_SESSION["step"]>$_GET['step']) {
  $step = $_GET['step'];
}

function getShortcut($STEPS,$shortcut){
  foreach ($STEPS as $key => $_steps){
    if(isset($_steps['shortcut']) && strtolower(trim($_steps['shortcut'])) == strtolower(trim($_GET['key']))){
      return $key; 
    } 
  }
}

if(isset($step)){
  $_SESSION["step"] = $step;
  $data = $STEPS[$step];
}
elseif(isset($_SESSION["step"]) && $_SESSION["step"] != 0){
  $data = $STEPS[$_SESSION["step"]];
  $step = $_SESSION["step"];
}
else{
  $data = $STEPS[1];
  $step = 1;
}

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>S & R</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/custom.css">

    <?php if(isset($data['final'])){ ?>
      <link type="text/css" rel="stylesheet" href="lightgallery/css/lightgallery.css" />
      <link type="text/css" rel="stylesheet" href="lightgallery/css/lg-zoom.css" />
      <link type="text/css" rel="stylesheet" href="lightgallery/css/lg-thumbnail.css" />
      <link type="text/css" rel="stylesheet" href="lightgallery/css/lightgallery-bundle.css" />
    <?php } ?>
	</head>

	<body>
    <?php 
      if(!isset($data['final']))
        include_once(__DIR__.'/utils/form.php'); 
      else 
        include_once(__DIR__.'/utils/final.php'); 
    ?>


    <?php if(isset($data['final'])){ ?>
      <script src="lightgallery/lightgallery.umd.js"></script>
      <!-- lightgallery plugins -->
      <script src="lightgallery/js/plugins/thumbnail/lg-thumbnail.umd.js"></script>
      <script src="lightgallery/js/plugins/zoom/lg-zoom.umd.js"></script>
      <script src="lightgallery/js/plugins/video/lg-video.umd.js"></script>
      <script type="text/javascript">
        // lightGallery(document.getElementById('animated-thumbnails-gallery'), {
        //     thumbnail: true,
        //     licenseKey: '01 00-0000-000-0000'
        // });
        lightGallery(document.getElementById('animated-thumbnails-gallery'), {
            plugins: [lgVideo],
            videojs: true,
            autoplayVideoOnSlide: true,
            animateThumb: false,
            zoomFromOrigin: false,
            allowMediaOverlap: true,
            toggleThumb: true,
        });

      </script>
    <?php } ?>

	</body>
</html>
      
